﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AutomationWithRestSharpNNUnit.Models
{
    public class BaseUser
    {
        public string name { get; set; }
        public string job { get; set; }
        public string id { get; set; }
    }

    public class CreateUser : BaseUser
    {
        public DateTime createdAt { get; set; }
    }

    public class UpdateUser : BaseUser
    {
        public DateTime updatedAt { get; set; }
    }

    public class ListOfUserResponse
    {
        public int page { get; set; }
        public int per_page { get; set; }
        public int total { get; set; }
        public int total_pages { get; set; }
        public List<UserVM> data { get; set; }
        public Support support { get; set; }
    }

    public class UserVM
    {
        public int id { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string avatar { get; set; }
    }

    public class Support
    {
        public string url { get; set; }
        public string text { get; set; }
    }

}
