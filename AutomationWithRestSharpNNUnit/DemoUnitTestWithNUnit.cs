using AutomationWithRestSharpNNUnit.Models;
using NUnit.Framework;
using RestSharp;
using System.Collections.Generic;
using System.Net;

namespace AutomationWithRestSharpNNUnit
{
    public class DemoUnitTestWithNUnit
    {
        [Test]
        public void ContentTypeTest()
        {
            // arrange
            RestRequest request = new RestRequest("nl/3825", Method.GET);

            // act
            var response = PerformRestRequest("http://api.zippopotam.us",request);

            // assert
            Assert.That(response.ContentType, Is.EqualTo("application/json"));
        }

        [Test]
        public void StatusCodeTest()
        {
            // arrange
            RestRequest request = new RestRequest("nl/3825", Method.GET);

            // act
            var response = PerformRestRequest("http://api.zippopotam.us", request);
            // assert
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [TestCase("nl", "3825", HttpStatusCode.OK, TestName = "Check status code for NL zip code 7411")]
        [TestCase("lv", "1050", HttpStatusCode.NotFound, TestName = "Check status code for LV zip code 1050")]
        public void StatusCodeTest(string countryCode, string zipCode, HttpStatusCode expectedHttpStatusCode)
        {
            // arrange
            RestClient client = new RestClient("http://api.zippopotam.us");
            RestRequest request = new RestRequest($"{countryCode}/{zipCode}", Method.GET);

            // act
            IRestResponse response = client.Execute(request);

            // assert
            Assert.That(response.StatusCode, Is.EqualTo(expectedHttpStatusCode));
        }

        [Test]
        public void Get()
        {
            // arrange
            RestRequest request = new RestRequest("api/users?page=2", Method.GET);
            // act
            var response = PerformRestRequest<ListOfUserResponse>("https://reqres.in", request);
            // assert
            Assert.That(response.Data.data.Count, Is.GreaterThan(1));
        }

        [Test]
        public void Post()
        {
            // arrange
            RestRequest request = new RestRequest("api/users", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(
                new CreateUser
                {
                    name = "Fahmida Nina",
                    job = "Software Engineer"
                });
            // act
            var response = PerformRestRequest<CreateUser>("https://reqres.in", request);
            // assert
            Assert.That(response.Data.name, Is.EqualTo("Fahmida Nina"));
        }

        [Test]
        public void Delete()
        {
            //client.Execute(request)

            // arrange
            RestRequest request = new RestRequest("api/users/{id}", Method.DELETE);
            request.AddParameter("id", 2);
            // act
            var response = PerformRestRequest("https://reqres.in", request);
            // assert
            Assert.That(response.StatusCode, Is.EqualTo(HttpStatusCode.NoContent));
        }

        [Test]
        public void Update()
        {
            //client.Execute(request)

            // arrange
            RestRequest request = new RestRequest("api/users/2", Method.PUT);
            request.AddJsonBody(
                new UpdateUser
                {
                    name = "Fahmida Nina",
                    job = "Fullstack Software Engineer"
                }
            );
            // act
            var response = PerformRestRequest<UpdateUser>("https://reqres.in", request);
            // assert
            Assert.That(response.Data.job, Is.EqualTo("Fullstack Software Engineer"));
        }

        public IRestResponse PerformRestRequest(string baseUrl, RestRequest request)
        {
            RestClient client = new RestClient(baseUrl);
            IRestResponse response = client.Execute(request);
            return response;
        }
        public IRestResponse<T> PerformRestRequest<T>(string baseUrl, RestRequest request)
        {
            RestClient client = new RestClient(baseUrl);
            IRestResponse<T> response = client.Execute<T>(request);
            return response;
        }
    }
}